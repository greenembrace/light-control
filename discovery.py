#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from gpiozero import Button,LED
from gpiozero.pins.pigpio import PiGPIOFactory
from time import sleep

class discovery(Button):
    def __init__(self,startPin,endPin,reservedPins,IP,led):
        self.buttons = {}
        self.buttonSet = set()
        self.discoveryMode = True
        self.startPin = startPin
        self.endPin = endPin
        self.reservedPinList = reservedPins
        self.index = 0
        self.IP = IP
        self.LED = led
        self.LEDPin = 0
    
    def flashLED(self):
        # indicate discovery fast flash LED for 10 sec's
        self.LED.blink(.15,.15,15)
        sleep (1)
        self.LED.blink(1,1,background=True)    

    def openPins(self):
        try:
            factory = PiGPIOFactory(host=self.IP)
        except OSError:
            quit()

        for pin in range(self.startPin,self.endPin):
            if pin in self.reservedPinList.keys(): continue # skip reserved pins
            
            try:
                self.buttons[self.index] = Button(pin,bounce_time=0.1,pin_factory=factory)
            except:
                continue    
            self.buttons[self.index].when_released = self.buttonReleased
            self.buttons[self.index].when_held = self.endDiscovery
            self.index = self.index + 1
  
    def buttonReleased(self,btn):
        self.buttonSet.add(btn.pin.number)
        if self.LED != False: self.flashLED()
            

    def start(self):
        self.openPins()
        while self.discoveryMode:
            pass

        self.closePins()    
        return self.buttonSet

    def endDiscovery(self,btn):
        self.buttonSet.add(btn.pin.number)
        if self.LED != False: self.flashLED()
        self.discoveryMode = False

    def closePins(self):
        for pin in range(0,self.index):
            try:
                self.buttons[pin].close()
            except:
                continue 
    



 



 
 
