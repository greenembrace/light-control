#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import print_function
import gpiozero
from requests import NullHandler
import configuration as config
import arrow 
from time import sleep
import datetime
import os, sys
import paho.mqtt.client as mqtt
from logzero import logger
import logzero
import ujson as json
from gpiozero import Button,LED
from gpiozero.pins.pigpio import PiGPIOFactory
import discovery
import identity as id
import socket  
from getmac import get_mac_address 
import  subprocess
import openmotics
from qhue import Bridge
import re

# TODO :
# TODO : add lock file to prevent multiprocess
# TODO : 
#
# *****************************************************************************
# try to open configuration file, no point continuing if can't!
# *****************************************************************************
logzero.logfile("Hal.log", maxBytes=1e5, backupCount=3)
try:
    config = config.configuration('Hal.yml')
    config.open()
except AssertionError as error:
    logger.error('Configuration: file has duplicate entries: ' + error)
    print(error)
    quit()

except Exception as error:
        logger.exception(error)
        logger.error('Configuration: file is corrupt or does not exist')
        if not os.path.exists("Hal.yml"):
            logger.error('Creating File: configuration "Hal.yml" does not exist')
            print('Creating File: configuration "Hal.yml" does not exist') 
            config.newConfigFile()   
            os.execv(sys.executable, ['python'] + sys.argv)

# success - we have a configuration file
systemName = config.getName()
identity = id.identity()
print (systemName + ' ('+identity.unodeIP+'): [Configuration Version: ' + str(config.getVersion()) + ']') 


# ******************************************************************************
# MQTT Setup & extension
# ****************************************************************************** 
class MQTTClient(mqtt.Client):
    def __init__(self,cname,**kwargs):
        super(MQTTClient, self).__init__(cname,**kwargs)
        self.last_pub_time=arrow.utcnow()
        self.topic_ack=[]
        self.run_flag=True
        self.subscribe_flag=False
        self.bad_connection_flag=False
        self.connected_flag=False
        self.disconnect_flag=False
        self.disconnect_time=0.0
        self.pub_msg_count=0
        self.unodeID = ''
        self.LWT = ''
        self.status = ''
        self.publishRetries = 4

MQTTServer = 'mqtt.harmers.lightblast.info'
MQTTPort = 1884 
MQTTv5 = 5
MQTT = MQTTClient(identity.unodeID,protocol=MQTTv5)
MQTT.connected_flag = False
mqttUnodeTelementry = "tele/unodes/"
mqttUnodeMessages = 'cmnd/unodes/' + identity.unodeID + '/#'
MQTT.LWT = mqttUnodeTelementry + identity.unodeID + '/LWT'

# *****************************************************************************   
# Button Setup & extension
# *****************************************************************************
class Button(Button):
    def __init__(self,cname,**kwargs):
        super(Button, self).__init__(cname,**kwargs)
        self.pressCount = 0
        self.lastUsed = arrow.utcnow()
        self.startTime = arrow.utcnow()
        self.timeout = 0
        self.dimmerMode = False
        self.pressSequence = ''
        self.currentValue = False
        self.openmoticsID = 0
        self.dimmerID = 0
        self.connected_flag=False

# ****************************************************************************
# GLOBAL Variables 
# ****************************************************************************

hueBridge = Bridge("192.168.1.18", '1AbvIMPyUIf0NYi9hBJsw7c2Bab8ZNACXdi9h0jQ')
UNODE = {}
discoveryMode = False
buttons = []
ON = True
OFF = False
DEBUG = False
logzero.loglevel(logzero.DEBUG)
loopTrue = True

om = openmotics.openmotics()

# **************************************************************************
# MQTT :  communications 
# **************************************************************************
def on_commandMessage(Client, userdata, message):
    pass

def on_broadcastMessage(Client, userdata, message):
    pass

def on_message(Client, userdata, message):
    global uNodeID, discoveryMode, startingPin,endingPin
    cmndMsgLen = len(mqttUnodeMessages)
    data = message.payload.decode("utf-8").replace("'", '"')
    topic = message.topic
    if 'cmnd' in topic:
         # some type of command message
        command = topic[cmndMsgLen-1:]
        if 'RESTARTDAMON' in command:
            startPiGPIO()
        
        elif 'DELETE' in command:
            pass
            #TODO : check if thread exists first

        elif 'UPDATECONFIG' in command:
            config.update(data)

        elif 'REBOOT' in command:
            # reboot program
            python = sys.executable
            os.execl(python, python, * sys.argv)

        elif 'DELETEPIN' in command:
            pass

        elif 'SHUTDOWN' in command:
            loopTrue = False
        
        elif 'DELETEUNODE' in command:
            pass    

        elif 'BUTTONDISCOVERY' in command:
            uNodeJson = json.loads(data)
            action = uNodeJson['action'].lower()
            # stop current running buttons
            stopPins(UNODE['pins'])
            discoverButtons(action)
         
    else:
            pass
            # ignore everything else nor valid command           
    
    # if enabled, i'm not sure why it should not be, but.....
    if re.search(r'cmnd/unodes/.*/enabled', topic):
        if 'enabled' in topic:
             UNODE['enabled'] = True if data.lower() == 'true' else False 


def on_publish(client,userdata,mid):
    MQTT.last_pub_time=arrow.utcnow()

def on_connect(client, userdata, flags, rc, properties):
    if rc == 0:
        MQTT.connected_flag = True
        mqttPublish(mqttUnodeTelementry + str(identity.unodeID) + '/LWT', 'Online', 2, True)
    
    else:
        logger.error('Connection refused : ' + str(rc))
        MQTT.connected_flag = False

def on_disconnect(client, userdata, rc):
    MQTT.connected_flag = False
    MQTT.disconnect_flag = True
    MQTT.disconnect_time = arrow.utcnow()
    if rc == mqtt.MQTT_ERR_SUCCESS:
        # user disconnect() called successfully
        logger.info('MQTT disconnected : ' + str(rc))

def mqttPublish(topic,data,qos=0,retain=False):
    unpublished = True
    tries = 0
    while unpublished and tries < MQTT.publishRetries:
        if MQTT.connected_flag == False:
            mqttConnect()

        ret = MQTT.publish(topic,data,qos=qos,retain=retain)
        if ret.rc == mqtt.MQTT_ERR_SUCCESS:
            unpublished = False
        else:
            tries = tries + 1

    return unpublished

def mqttConnect():
    try:
        MQTT.will_set(MQTT.LWT, 'Offline', 1, True)
        ret = MQTT.connect(host=MQTTServer, port=MQTTPort, keepalive=60)
        if ret == mqtt.MQTT_ERR_SUCCESS:
            MQTT.connected_flag = True
            MQTT.on_message = on_message
            MQTT.on_connect = on_connect
            MQTT.on_disconnect = on_disconnect
            MQTT.on_publish = on_publish

            MQTT.subscribe(mqttUnodeMessages, qos=2)

    except ConnectionRefusedError:
        logger.error('MQTT Server refusing connection or Invalid Credentials')
        MQTT.connected_flag = False
        
    except Exception as error:
            logger.exception(error)

# *********************************************************************************
# NodeRed - unode to NR
# *********************************************************************************
def updateNR(buttonID):
    # update the node-red system via mqtt
    button = UNODE['pins'][buttonID]
    try:
        message =str({"openmoticsID": button['openmoticsID'],
                    "dimmerID": button['dimmerID'],
                    "pin": buttonID,
                    "currentValue": button['device'].currentValue,
                    "pressCount": button['device'].pressCount,
                    "type": button['type'],
                    "multiPress": button['multiPress'],
                    "dimmerValue": button['dimmerValue'],
                    "command" : button['command'],
                    "lastPress" : button['device'].startTime.format('YYYY-MM-DD HH:mm:ss ZZ'),
                    "previousToLast" : button['device'].lastUsed.format('YYYY-MM-DD HH:mm:ss ZZ')})

        topic = mqttUnodeTelementry + str(identity.unodeID) + '/buttons/' + str(buttonID)
        
        ret = mqttPublish(topic,message,qos=0,retain=False)
        if ret != mqtt.MQTT_ERR_SUCCESS:
            print ('publish failed')
            
    except Exception as error:
        logger.exception(error)
        return

# ********************************************************************************
# UNODE - load data
# ********************************************************************************
def loadUnode():
    if not config.pinCount(): # 'No pin definitions in configration'
        return False

    if MQTT.connected_flag: # if mqtt is up see if unode is enabled
        assert not ('enabled' in UNODE and UNODE['enabled'] == False), 'unode not enabled'

    try:
        activePins = 0
        for pinID in UNODE['pins']: # look through the buttons in the unode record
            # active the pin on the unode for each button return the pin handle
            pin = UNODE['pins'][pinID]
            newPin = addPin(pinID,pin)
            if newPin:
                # add entry for each pin to the unode record
                UNODE['pins'][pinID]['device'] = newPin
                UNODE['pins'][pinID]['multiPress'] = False
                UNODE['pins'][pinID]['dimmerDirection'] = 'up'
                UNODE['pins'][pinID]['shortCount'] = 0
                UNODE['pins'][pinID]['dimmerValue'] = 0
                UNODE['pins'][pinID]['command'] = 'empty'

            continue # next button
 
        UNODE['activePins'] = activePins

    except Exception as error:
        logger.exception(error)
        return False
    return True    

def systemSettings():
    x = config.getUnode()
    UNODE['pins'] = x['pins']
    UNODE['debounce'] = x['debounce']
    UNODE['dimmerPressCompletion'] = x['dimmerPressCompletion']
    UNODE['longButtonCompletion'] = x['longButtonCompletion']
    UNODE['shortButtonCompletion'] = x['shortButtonCompletion']
    UNODE['startPin'] = x['startPin']   
    UNODE['endPin'] = x['endPin']
    UNODE['reservedGPIOPins'] = x['reservedGPIOPins']
    UNODE['dimmerSpeed'] = x['dimmerSpeed']

    # add activity flash and power LED 
    led = config.getReservedPin('activityLED')
    if led:
        try:
             UNODE['activityLED'] = LED(led)
        except:
             UNODE['activityLED'] = False

    # add power led indicates the program is running
    led = config.getReservedPin('powerLED')
    if led:
        try: 
            UNODE['powerLED'] = LED(led)
        except:    
            UNODE['powerLED'] = False

# *******************************************************************************
# pin configuration 
# *******************************************************************************
def addPin (pinName,pin):
    buNode = identity.unodeID
    bPin = pinName
    bOM = pin['openmoticsID']
    bDPC = UNODE['dimmerPressCompletion']
    bDebounce = UNODE['debounce']
    pinList = config.getPinList()

    try:
        config.parameterValidation(buNode, bPin, bOM, pinList)
        try:
            return setupPin(bPin, bDebounce, bDPC)
        except:
            logger.warning('Unable to start button: ' + buNode + ' : ' + str(bPin))
    except ValueError as error:
        print('Unable to start button, invalid parameters:(' + str(error) +') '+ buNode + ' : ' + str(bPin))
        logger.warning('Unable to start button, invalid parameters:(' + str(error) +') '+ buNode + ' : ' + str(bPin))
        return False
    except Exception as error:
        logger.exception(error)
        print('Unable to start button, error:(' + str(error) +') '+ buNode + ' : ' + str(bPin)) 
        logger.warning('Unable to start button, errors:(' + str(error) +') '+ buNode + ' : ' + str(bPin)) 
        return False


def setupPin(pin, debounce, DPC):
    try:
        factory = PiGPIOFactory(host='localhost')
        button = Button(pin,bounce_time=debounce,pin_factory=factory)
        button.when_pressed = lambda : buttonPressed(pin)
        button.when_released = lambda : buttonReleased(pin)
        button.when_held = lambda : dimmerPress(pin)
        button.hold_time = DPC
        button.hold_repeat = True
        return button
        
    except Button.PinSetInput as error:
        print ('Error: Pin Set Input')
        logger.warning('Error: attempting to set a read-only pin:(' + str(error) +') '+ identity.unodeID + ' : ' + str(pin))    
    except Exception as error:
        logger.exception(error) 
        logger.warning('Unable to start button, errors:(' + ') '+ identity.unodeID + ' : ' + str(pin)) 
    return False   
  
def stopPins(pins):
    try:
        for pin in pins: # look through the buttons in the unode record
            # active the pin on the unode for each button return the pin handle
            pin.close()
            
    except Exception as error:
        logger.exception(error)

def pinAction(pinDiscoverySet,action):
    global discoveryMode

    try:

        # see if there is already an entry in configuration
        uNode = config.getUnode()

        if uNode and action == 'update':
            # update only the buttons pressed on existing unode
            config.updateButton(pinDiscoverySet)
            config.save()
                
        elif action == 'newUnode':
            config.addNewButton(pinDiscoverySet)
            config.save()

        elif action == 'delete':
            # delete old uNode entry if exists
            pass          
    
    except Exception as error:
        logger.exception(error)

    return

def discoverButtons(action):
    global discoveryMode

    print ( 'Starting button discovery')
    discoveryMode = True
    pinDiscoverySet = set()

    # if the uNode has not been enabled skip
    if UNODE['enabled'] == False: return False
     # start flashing the discovery LED
    if UNODE['activityLED'] != False:
        UNODE['activityLED'].blink(1,1,background=True)
   

    startingPin = config.getStartPin()
    endingPin = config.getEndPin()
    probe = discovery.discovery(startingPin,endingPin,config.getReservedGPIOPins(),identity.unodeIP,UNODE['activityLED'])
    pinDiscoverySet = probe.start()
    if len(pinDiscoverySet)>=1:
        pinAction(pinDiscoverySet,action)
        discoveryMode = False
        print ( 'Ending button discovery') 
        UNODE['pins'] = config.getUnode()['pins']
        loadUnode()
        UNODE['activityLED'].close()

        return True
    else:
        return False     

# ****************************************************************************
# BUTTON - management 
# ****************************************************************************
def buttonPressed(buttonID):    
    if UNODE['activityLED'] != False:    
        UNODE['activityLED'].blink(1,1,1)
        
    # when button first pressed and not yet release
    UNODE['pins'][buttonID]['device'].pressCount = 1
    UNODE['pins'][buttonID]['device'].dimmerMode = False
    UNODE['pins'][buttonID]['device'].lastUsed = UNODE['pins'][buttonID]['device'].startTime
    UNODE['pins'][buttonID]['device'].startTime = arrow.utcnow()

            
def buttonReleased(buttonID):
    button = UNODE['pins'][buttonID]

    if UNODE['pins'][buttonID]['device'].dimmerMode:
        UNODE['pins'][buttonID]['device'].dimmerMode = False
        if UNODE['pins'][buttonID]['dimmerDirection'] == 'up':
            UNODE['pins'][buttonID]['dimmerDirection'] = 'down'
        else:
            UNODE['pins'][buttonID]['dimmerDirection'] = 'up'    
        return

    try:
        sBC = datetime.timedelta(seconds=UNODE['shortButtonCompletion'])
        lBC = datetime.timedelta(seconds=UNODE['longButtonCompletion'])
        # duration <= lBC = normal Press
        delay = arrow.utcnow() - button['device'].startTime
        if delay <= sBC:
                # normal button press
                toggleOpenmotics(buttonID)

        elif delay <= lBC: # lBC < duration < dimmerPress
            if UNODE['pins'][buttonID]['multiPress']:
                # might be fan toggle
                dID = UNODE['pins'][buttonID]['dimmerID']
                hueBridge.lights[dID].state(alert='lselect')
                pass
 
            else:
                # see if button or function
                if UNODE['pins'][buttonID]['type'].lower() == 'function':
                    # this is a functional press
                    buttonFunction(buttonID)
                else:
                    # otherwise just treat it as a normal press
                    toggleOpenmotics(buttonID)  
    
    except Exception as error:
        logger.exception(error)
        return

# *****************************************************************************************
# Button Functions
# *****************************************************************************************

#### THIS FUNCTION IS NOT FINISHED #####

def buttonFunction(buttonID):
    try:
        function = UNODE['pins'][buttonID]['command'].lower()
        if function == 'nil':
            # no function here!
            return
        else:
            # do something interesting
            pass
    
    except Exception as error:
        logger.exception(error)
        return    

# *****************************************************************************************
# Dimmer functions
# *****************************************************************************************

def dimmerPress(buttonID):
    UNODE['pins'][buttonID]['device'].dimmerMode = True
    dID = UNODE['pins'][buttonID]['dimmerID']
    incrementValue = UNODE['dimmerSpeed']
    #print(UNODE['pins'][buttonID]['device'].active_time)
    try:
        # check that this device is a valid dimmer
        if dID is not 0:
            UNODE['pins'][buttonID]['device'].pressCount += 1
            #print (str(UNODE['pins'][buttonID]['device'].pressCount))
            if UNODE['pins'][buttonID]['type'].lower() == 'philips hue':
                # turn physical light 'on' if it's 'off'
                turnOn(buttonID)
                dimmerValue = hueBridge.lights[dID]()['state']['bri']

            direction = UNODE['pins'][buttonID]['dimmerDirection']

            if direction == 'up':
                dimmerValue += incrementValue
                if dimmerValue > 254:
                    dimmerValue = 254
                    UNODE['pins'][buttonID]['dimmerDirection'] = 'down'

            else: # direction == 'down'
                dimmerValue -= incrementValue
                if dimmerValue < 0:
                    dimmerValue = 0
                    UNODE['pins'][buttonID]['dimmerDirection'] = 'up'

            updateDimmer(buttonID, dimmerValue)
            UNODE['pins'][buttonID]['dimmerValue'] = dimmerValue

    except Exception as error:
        logger.exception(error)
        return

def updateDimmer(buttonID, dimmerValue):
    # update Openmotics system
    # TODO add serial RS485 update back to base controller
    
    try:
        # turn physical light 'on' if it's 'off'
        turnOn(buttonID)
        
        # adjust the virtual dimmer
        params = {'is_on':True}
        params['id'] = UNODE['pins'][buttonID]['dimmerID']
        params['dimmer'] = dimmerValue
        if UNODE['pins'][buttonID]['type'].lower() == 'philips hue':            
            result = hueBridge.lights[UNODE['pins'][buttonID]['dimmerID']].state(on=True,bri=dimmerValue,transitiontime=300)

        
        if not result:
            logger.error('API: endpoint call failed')
        else:
            updateNR(buttonID)

        return

    except Exception as error:
        logger.exception(error)
        return

# ******************************************************************************
# OPENMOTICS - communication 
# ******************************************************************************
def getCurrentStatus(openmoticsID):
    result =  json.loads(om.callAPI(endpoint='get_output_status'))['status'][openmoticsID]['status']
    #print(om.callAPI(endpoint='get_output_status'))
    return result

def getCurrentDimmer(dimmerID):
    result = json.loads(om.callAPI(endpoint='get_output_status'))['status'][dimmerID]['dimmer']
    return result

def turnOn (buttonID):
    # turn physical light 'on' if it's 'off'
    openmoticsID = UNODE['pins'][buttonID]['openmoticsID']
    UNODE['pins'][buttonID]['device'].currentValue = True
    params = {'is_on':True}
    params['id'] = openmoticsID
    json.loads(om.callAPI(endpoint='set_output',params=params))['success']

def toggleOpenmotics(buttonID, state=ON):
    # update Openmotics system, turn light on/off
    openmoticsID = UNODE['pins'][buttonID]['openmoticsID']
    try:
        params = {'dimmer':100}
        params['id'] = openmoticsID
        # False = 'OFF' True = 'ON'
        if getCurrentStatus(openmoticsID) == False:
            UNODE['pins'][buttonID]['device'].currentValue = True
            params['is_on'] = True
        else:
            UNODE['pins'][buttonID]['device'].currentValue = False
            params['is_on'] = False   
        
        # check 'success' from  API call
        if not json.loads(om.callAPI(endpoint='set_output',params=params))['success']:
            logger.error('API: endpoint call failed to toggle light')

        updateNR(buttonID)

    except Exception as error:
        logger.exception(error)
        return

# ******************************************************************************
# PIGPIO - Damon
# ******************************************************************************
def startPiGPIO():
    try:
        # if the damon process if running kill it and/or restart 
        #result = subprocess.run(["ps -ef | grep pigpiod | awk '{print $2}'  | sudo xargs kill -9"], shell=True, capture_output=True, text=True )
        result = subprocess.run(['sudo','pigpiod'],capture_output=True, text=True )
        a=1
    finally:
        logger.info('pigpiod: ' + result.stderr)

# ********************************************************************************
# publish identification
# ********************************************************************************
def publishIdentity():
    message = str({"unodeMAC" : identity.unodeID, "uNodeIP" : identity.unodeIP, "uNodeURL": identity.unodeURL, "hostname" : identity.hostname, "timestamp" : arrow.now().format('YYYY-MM-DD HH:mm:ss ZZ')})
    topic = mqttUnodeTelementry + str(identity.unodeID) + '/discovery'
    mqttPublish(topic,message,qos=0,retain=True)

    pi = gpiozero.pi_info()
    message =str({'model':pi.model,'revision':pi.pcb_revision,'released':pi.released,
                    'soc':pi.soc,'memory':pi.memory,'storage':pi.storage,'usb':pi.usb,'usb3':pi.usb3,
                    'ethernet':pi.ethernet,'ethSpeed':pi.eth_speed,'wifi':pi.wifi,'bluetooth':pi.bluetooth,
                    'camera':pi.csi,'display':pi.dsi})
    topic = mqttUnodeTelementry + str(identity.unodeID) + '/piInfo'                
    mqttPublish(topic,message,qos=0,retain=True)

def publishConfigFile():
    message = str(config.data)
    topic = mqttUnodeTelementry + str(identity.unodeID) + '/config'
    mqttPublish(topic,message,qos=0,retain=True)

def systemExit(message):
    try:
        print('\nClosing solenoids: ' + str(message))
        logger.info('Shutting down: ' + arrow.now().format('YYYY-MM-DD HH:mm:ss ZZ'))
    except Exception as error:
        try:
            logger.exception('Shutting down: ' + error)
        except:
            # logger not working just give up
            pass

    try:
        sys.exit(0)
    except SystemExit:
        os._exit(0)

# *********************************************************************************
#  main 
# *********************************************************************************
def main():
    # start mqtt wait and check for input ans short wait to allow messages to arrive
    mqttConnect()
    MQTT.loop_start() 
    sleep(10)  

    # let the world know who we are....
    publishIdentity()

    # startPiGPIO (pigpio damon) - must come before unode loaded and pins accessed
    startPiGPIO()               

    # load buttons for the unodes from the configuration file
    # and start the pigpio damon. flash the on-line LED
    # if there are no buttons run button discovery automatically
    # until postive result 
    systemSettings()
    while not loadUnode():
         discoverButtons('newUnode')
    
    # publist the configuration fle. elements may need to be hand massaged
    publishConfigFile()

    # flash the on-line LED to indicate the program is running     
    if UNODE['powerLED'] != False:
        UNODE['powerLED'].blink(1,2,background=True)

    # wait here for button to be pressed or message to arrive via mqtt
    logger.info('Starting loop: ' + arrow.now().format('YYYY-MM-DD HH:mm:ss ZZ'))
    print('Starting loop: ' + arrow.now().format('YYYY-MM-DD HH:mm:ss ZZ'))
    while loopTrue:
        pass

    systemExit('shutting down....')

if __name__ == '__main__':
    try:
      main()

    except KeyboardInterrupt:
        systemExit('Keyboard interrupt')

    except Exception as error:
        systemExit(error)