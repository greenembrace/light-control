#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import identity as id
from ruamel.yaml import YAML
import ujson as json
from logzero import logger

identity = id.identity()

yaml=YAML(typ='safe')
yaml.indent(mapping=4)
yaml.default_flow_style = False
yaml.preserve_quotes = True

class configuration():
  def __init__(self,file):
    self.configFile = file


  # ************************************************************
  # File manipulation
  # ************************************************************

  def open(self):
    try:
      with open(self.configFile, 'r') as f:
        self.data = yaml.load(f)
        self.unode = self.data['unode']
        self.fileHandle = f
        self.file = self.configFile
    except:
      raise FileExistsError("Configuration file does not exist")

  def write(self,file,yamlData):
    # write a new YAML config file
    with open(file, 'w') as f:
        yaml.dump(yamlData, f)

  def save(self):
    # write data back to config file. return true if successful
    #self.data['unode'] =  self.unode['data']['unode']
    try:
        with open(self.configFile, 'w') as f:
          yaml.dump(self.data, f)
          return True
    except:
      return False        

  def read(self,file):
    # Read YAML file
    with open(file, 'r') as f:
        return yaml.load(f)

  def update(self,newConfig):
    # replace the current data with a new data set
    try:
      with open(self.file, 'w') as f:
        yaml.dump(newConfig, f) 
        self.data = newConfig
        return True
    except:
      return False

  def close(self):
    # close configuration file
    try:
      self.fileHandle.close()   
      return True
    except:
      return False

  def newConfigFile(self):
    try:
      with open(self.configFile, 'x') as f: 
        config = """\
          harmers: 
            version: 20.0
            openmotics: 
              login: daniel
              password: Ds3ppF14ha
              port: 443
              server: 192.168.1.20
            name: Undefined System  
          unode: # settings
            debounce: 0.05
            dimmerPressCompletion: 1.0
            dimmerSpeed: 50
            startPin: 2
            endPin: 27
            shortButtonCompletion: 0.4
            longButtonCompletion: 0.95
            reservedGPIOPins:
              list: {2: null, 3: null, 4: null, 14: null, 16: null, 17: null}
              Rx: 16
              Tx: 14
              powerLED: 4
              activityLED: 27
              sda: 2
              sdl: 3
            pins: # physical available pins
        """
        yaml.dump(yaml.load(config),f)    
        
    except:
      raise FileExistsError("Configuration file: can't be created")

  # *************************************************************
  # tools
  # *************************************************************

  def yaml2Json(self,data,sortkeys=False):
    # output YAML object to JSON format
    try:
      return json.dumps(data, sort_keys=sortkeys)
    except:
      pass

  def json2Dict(self,jsonData):
    # JSON data into Python Dict
    try:  
      return json.load(jsonData)
    except:
      pass

  def data(self,jsonData):
    # JSON data into Python Dict
    try:  
      return self.data
    except Exception as error:
      print(error)  
  
  # ***************************************************************
  # Configuration Header
  # ***************************************************************

  def getOpenmoticsServer(self):
    # return the openmotics data
    try:
      return self.data['harmers']['openmotics']
    except:
      return False  

  def getVersion(self):
    # return the data version
    try:
      return self.data['harmers']['version']
    except:
      return False

  def getName(self):
    # return the system name
    try:
      return self.data['harmers']['name']
    except:
      return 'Undefined System'

  def setVersion (self,version):
    # set the version value
    try:
      self.data['harmers']['version'] = version
      return True
    except:
      return False

  def setOpenmoticsServer (self, openmoticsServer='', openmoticsPort=0, openmoticsLogin = '', openmoticsPassword = ''):
    # set the openmotics data
    try:
      openmotics = self.getOpenmoticsServer()
      if openmoticsServer != '':
        openmotics['server'] = openmoticsServer
      if openmoticsPort > 0:
        openmotics['port'] = openmoticsPort
      if openmoticsLogin != '':
        openmotics['login'] = openmoticsLogin
      if openmoticsPassword != '':
        openmotics['password'] = openmoticsPassword 

      self.data['harmers']['openmotics'] = openmotics
    except:
      return False    

  
  # **************************************************************
  # uNode manipulation
  # **************************************************************

  
  def getUnode(self):
    # return uNode with uNodeID 
    try:
      return self.data['unode']
    except:
      return False

  def updateUnode(self,newUnode):
    # update the unodeInfo data
    try:
      self.data['unode'] = newUnode    
    except:
      return False

  def getDebounce(self):
    return self.data['unode']['debounce']
  
  def getTimeout(self):
    return self.data['unode']['timeout']
  
  def getShortButtonCompletion(self):
    return self.data['unode']['shortButtonCompletion']

  def getLongButtonCompletion(self):
    return self.data['unode']['longButtonCompletion']

  def getdimmerPressCompletion(self):
    return self.data['unode']['dimmerPressCompletion']
  
  def getdimmerSpeed(self):
    return self.data['unode']['dimmerSpeed']

  def getPinList(self):
    # return the valid and available pins
    pinList = {}
    try:
      pinList = self.data['unode']['pins'].keys()
      return pinList
    except:
      return False

  def getStartPin(self):
    return self.data['unode']['startPin']
    
  def getEndPin(self):
    return self.data['unode']['endPin']

  def getReservedGPIOPins(self):
    return self.data['unode']['reservedGPIOPins']['list']    

  def getSDA(self):
    return self.data['unode']['reservedGPIOPins']['sda']

  def getSDL(self):
    return self.data['unode']['reservedGPIOPins']['sdl']

  def getTx(self):
    return self.data['unode']['reservedGPIOPins']['Tx']

  def getRx(self):
    return self.data['unode']['reservedGPIOPins']['Rx']

  def getPowerLED(self):
    return self.data['unode']['reservedGPIOPins']['powerLED']

  def getActivityLED(self):
    return self.data['unode']['reservedGPIOPins']['activityLED']

  # *************************************************************
  # Button/Pin Manipulation
  # *************************************************************

  def addNewButton(self,buttonPins):
    # adds new button with defaults
    for pin in buttonPins:
      button = {}
      button['openmoticsID'] = 0
      button['type'] = 'default'
      button['dimmerID'] = 0
      button['multiPress'] = False
      button['command'] = 'empty'
      try:
        self.addButton(pin,button)
      except Exception as error:
          logger.exception(error)

  def addButton(self,buttonName,button):
    try:
      # add a new button with supplied data
      if not self.buttonExists(buttonName):
        if self.data['unode']['pins'] == None:
          self.data['unode']['pins'] = {}
        self.data['unode']['pins'][buttonName] = button
        return True
      else:
        return False
    except Exception as error:
        logger.exception(error)

  def getAllPins(self):
    # get all the Pins or 'None' if they don't exist
    try:
      return self.data['unode']['pins']
    except:
      return False

  def getPin(self,pin):
    # return the specified pin or 'None' if it does not exist
    try:
       return self.data['unode']['pins'][pin]
    except:
      return False

  def delPin(self,pin):
    # delete a pin
    try:
      self.data['unode']['pins'].pop(pin)
      return True
    except:
      return False  

  def delAllPins(self):
    # delete all the pins
    try:
      self.data['unode'].pop('pins')
      return True
    except:
      return False  

  def changeButton(self,buttonName,newButton):
    # change a single button
    if self.delButton(buttonName):
      self.addButton(buttonName,newButton)
      return True
    else:
      return False

  def copyButton(self,buttonName1,buttonName2):
    # copy details of button1 to button2
    b1 = self.getButton(buttonName1)
    return self.addButton(buttonName2,b1)

  def pinCount(self):
    # return the number of pins
    try:
      return len(self.data['unode']['pins']) 
  
    except:
      return 0


  def buttonExists(self,buttonName):
    # check if button exists in buttons
    if self.data['unode']['pins'] == None: return False
    try:
      button = True if buttonName in self.data['unode']['pins'] else False 
      return button
    except:
      return False

  def updateButtons(self,buttons):
    # reload buttons into unode
    try:
      self.data['unode']['pins'] = buttons
      return True
    except:
      return False

  def getDimmable(self,pin):
    # reload buttons into unode
    try:
      return self.data['unode']['pins'][pin]['dimmable']
    except:
      return False

     
  def sortedButtons(self,direction = False):
    # returns a sorted list of button names. Optional direction, default descending
    return sorted(self.data['unode']['pins'],reverse = direction)

# ******************************************************************************
# T O O L S
# ******************************************************************************

  def getReservedPin (self, descriptor):
        pin = self.data['unode']['reservedGPIOPins'][descriptor]
 
        return pin if pin >= 0 else False

  def checkPin(self,bPin, bUnit, pinList):
    if not type(bPin) is int:
      raise ValueError ('Pin on: ' + bUnit + ' : [' +str(bPin) +'] is not "INTEGER"')
        
    if not bPin in range(2,27):  
      raise ValueError ('Pin on: ' + bUnit + ' : [' +str(bPin) +'] is "OUT OF RANGE"')
    
    if not bPin in pinList:  
      raise ValueError ('Pin on: ' + bUnit + ' : [' +str(bPin) +'] is not "VALID"')
    

  def checkOM(self,bOM, bUnit, bPin):
    if not type(bOM) is int:
        raise ValueError ('Openmotics Input: (' + bUnit + ' : ' + str(bPin) + ')[' + str(bOM) + '] is not Integer') 
          
    if not bOM in range (0,103):
      raise ValueError ('Openmotics Input: (' + bUnit + ' : ' + str(bPin) + ')[' + str(bOM) + '] is "OUT OF RANGE"')

  def parameterValidation(self, bUnit, bPin, bOM, pinList):
    self.checkPin(bPin, bUnit, pinList)
    self.checkOM(bOM, bUnit, bPin)
