#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import ujson as json
from urllib3.exceptions import InsecureRequestWarning
import configuration as config
from logzero import logger
import logzero

requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS='DEFAULT:@SECLEVEL=1'

logzero.logfile("Hal.log", maxBytes=1e5, backupCount=3)
logzero.json()

class openmotics():    
    def __init__(self):
        self.TOKEN = ''

        try:
            self.config = config.configuration('Hal.yml')
            self.config.open()
            self.openmoticsInfo = self.config.getOpenmoticsServer()
        except:
            logger.error('configuration: Hal.yml is invalid or "OPENMOTICS" section does not exist')
            quit()

        self.openmoticsServer = self.openmoticsInfo['server']
        self.openmoticsPort = self.openmoticsInfo['port']

        if self.openmoticsServer == '' or self.openmoticsPort == 0:
            logger.error ( 'No valid OPENMOTICS server or port')
            quit()
        else:    
            self.BASE_URL = 'https://' + self.openmoticsServer + ':' + str(self.openmoticsPort) + '/'

        self.openmoticsLogin = self.openmoticsInfo['login']

        if self.openmoticsLogin == '' :
            logger.error ( 'No valid login to Openmotics')
            quit()
        else:
            self.USERNAME = self.openmoticsLogin

        self.openmoticsPassword = self.openmoticsInfo['password']

        if self.openmoticsPassword == '' :
            logger.error ( 'No valid password to Openmotics')
            quit()
        else:
            self.PASSWD = self.openmoticsPassword    
        
    def callAPI(self, endpoint, params=None):
        try:
            if self.TOKEN != '':
                return self.api_call(endpoint=endpoint, params=params, authenticated=True)
        finally:    
            self.login()  
            return self.api_call(endpoint=endpoint, params=params, authenticated=True)
        

    def create_url(self, endpoint):
        url = '{}{}'.format(self.BASE_URL, endpoint)
        return url


    def api_call(self, endpoint, method='get', headers=None, params=None, body=None, authenticated=True):
        if headers is None:
            headers = {}

        if self.TOKEN != '' and authenticated:
            headers.update({'Authorization': 'Bearer {}'.format(self.TOKEN)})

        method = method.lower()
        url = self.create_url(endpoint)

        if params is None:
            params = {}

        response = requests.get(url=url, headers=headers, params=params, verify=False)
        resp_body = '\n'.join([str(x.decode()) for x in response.iter_lines()])

        return resp_body
    
    def login(self):
        try:
            if self.TOKEN != '':
                return self.TOKEN
        finally:    
            
            params = {'username': self.USERNAME, 'password': self.PASSWD}
            resp = self.api_call('login', params=params, authenticated=False)
            resp_json = json.loads(resp)
            if 'token' in resp_json:
                token = resp_json['token']
                self.TOKEN = token
                
            else:
                logger.error('openmotics login failed')
                raise RuntimeError('Could not log into Openmotics gateway : ' + self.BASE_URL)
            
            return token
   

   