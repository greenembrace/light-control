#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import requests
import ujson as json
from urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS='DEFAULT:@SECLEVEL=1'

class openmotics():    
    def __init__(self):
        self.TOKEN = ''
        self.openmoticsServer = '192.168.1.157'
        self.openmoticsPort = '443'
        self.BASE_URL = 'https://' + self.openmoticsServer + ':' + str(self.openmoticsPort) + '/'
        self.USERNAME = 'applogin'
        self.PASSWD = 'Ds3+pF14app'
        
    def callAPI(self, endpoint, params=None):
        try:
            if self.TOKEN != '':
                return self.api_call(endpoint=endpoint, params=params, authenticated=True)
        finally:    
            self.login()  
            return self.api_call(endpoint=endpoint, params=params, authenticated=True)
        

    def create_url(self, endpoint):
        url = '{}{}'.format(self.BASE_URL, endpoint)
        return url


    def api_call(self, endpoint, method='get', headers=None, params=None, body=None, authenticated=True):
        if headers is None:
            headers = {}

        if self.TOKEN != '' and authenticated:
            headers.update({'Authorization': 'Bearer {}'.format(self.TOKEN)})

        method = method.lower()
        url = self.create_url(endpoint)

        if params is None:
            params = {}

        response = requests.get(url=url, headers=headers, params=params, verify=False)
        resp_body = '\n'.join([str(x.decode()) for x in response.iter_lines()])

        return resp_body
    
    def login(self):
        try:
            if self.TOKEN != '':
                return self.TOKEN
        finally:    
            
            params = {'username': self.USERNAME, 'password': self.PASSWD}
            resp = self.api_call('login', params=params, authenticated=False)
            resp_json = json.loads(resp)
            if 'token' in resp_json:
                token = resp_json['token']
                self.TOKEN = token
            else:
                logger.error('openmotics login failed')
                raise RuntimeError('Could not log into Openmotics gateway : ' + self.BASE_URL)
            return token
   
om = openmotics()

def getStatus():
    result = om.callAPI(endpoint='get_output_status')
    return result

def getLightStatus(ID):
    result =  json.loads(om.callAPI(endpoint='get_output_status'))['status'][ID]['status']
    return result

def turnOn (ID):
    params = {'is_on':True}
    params['id'] = ID
    json.loads(om.callAPI(endpoint='set_output',params=params))['success']

def toggleOpenmotics(ID):
    params = {'dimmer':100}
    params['id'] = ID
    
    if getLightStatus(ID) == False:
        params['is_on'] = True
    else:
        params['is_on'] = False   
    
    # check 'success' from  API call
    if not json.loads(om.callAPI(endpoint='set_output',params=params))['success']:
        logger.error('API: endpoint call failed to toggle light')


ID = 50
getStatus()
getLightStatus(ID)
turnOn(50)