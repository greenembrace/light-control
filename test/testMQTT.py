import paho.mqtt.client as mqtt

mqttServer = 'mqtt.harmers.lightblast.info'
mqttPort = 1884 #1884
class MQTTClient(mqtt.Client):
   
   def __init__(self,cname,**kwargs):
      super(MQTTClient, self).__init__(cname,**kwargs)
      self.last_pub_time=time.time()
      self.topic_ack=[]
      self.run_flag=True
      self.subscribe_flag=False
      self.bad_connection_flag=False
      self.connected_flag=False
      self.disconnect_flag=False
      self.disconnect_time=0.0
      self.pub_msg_count=0
      self.devices=[]

MQTTClient = mqtt.Client('discovery',clean_session=False)


mqttControllerBaseTelementry = "tele/controller/"
mqttCommandMessages = "cmnd/controller/" 
mqttTelementryMaster = mqttControllerBaseTelementry + "master"
mqttUnitDiscovery = 'controller/discovery/#' 

def on_message(MQTTClient, userdata, message):
    global unitID, mqttButtonsMessages, mqttCommandMessages, unitOnline
    cmndMsgLen = len(mqttCommandMessages)
    data = message.payload.decode("utf-8").replace("'", '"')
    topic = message.topic
    print (topic)
    if topic[cmndMsgLen-3:] == 'LWT':
        print(topic)            
        # pick up LWT
        if data == "online":
            unitOnline = True
            print ('Online')
        else:
            unitOnline =  False
            print ('Offline')

    if topic[0:cmndMsgLen-1] == mqttCommandMessages[0:-1]:
       # command message
       pass
            # ignore everything else

               

def on_connect(MQTTClient, userdata, flags, rc):
    if rc == 0:
        print("MQTT connected OK Returned code: " + str(rc))
        MQTTClient.publish(mqttTelementryMaster +'/LWT', 'Online', qos=1, retain=True)
    elif rc == 1:
            print("Connection refused: incorrect protocol version" + str(rc))
    elif rc == 2:
            print("Connection refuse: invalid client identifier" + str(rc))
    elif rc == 3:
            print("Connection refused: server unavailable" + str(rc))
    elif rc == 4:
            print("Connection refused: bad username or password" + str(rc))
    elif rc == 5:
            print("Connection refused: not authorised" + str(rc))                    
    else:
        print("Connection refused: Bad connection :", str(rc))
    
def mqttConnect():
    global mqttButtonsMessages, mqttCommandMessages
    MQTTClient.will_set(mqttTelementryMaster +'/LWT', 'Offline', qos=1, retain=True)

    try:
        MQTTClient.connect(mqttServer, int(mqttPort), 60)
    except ConnectionRefusedError:
        print('MQTT Server refusing connection or Invalid Credentials: trying ini')
        return False

    # callbacks
    MQTTClient.on_message = on_message
    MQTTClient.on_connect = on_connect
    # subscriptions:
    #   mqttUnitDiscovery - when a unit comes on line
    #   mqttControllerBaseTelementry + "?/LWT/#" - unit Last Will. (offline)
    #   mqttCommandMessages - commands sent to the controller   
    MQTTClient.subscribe(mqttUnitDiscovery,qos=1)
    MQTTClient.subscribe(mqttControllerBaseTelementry + "+/LWT/#",qos=2)
    MQTTClient.subscribe(mqttCommandMessages,qos=1)
    return True    
    
mqttConnected = mqttConnect()
MQTTClient.loop_forever()