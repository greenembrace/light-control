

import time
from gpiozero import LED,Button
from gpiozero.pins.pigpio import PiGPIOFactory
factory = PiGPIOFactory(host='192.168.1.47')

red = LED(2,pin_factory=factory)

try:
    yellow = LED(4,pin_factory=factory)
except:
    pass

green = LED(3,pin_factory=factory)

rb = Button(16,pin_factory=factory)
yb = Button(20,pin_factory=factory)
gb = Button(21,pin_factory=factory)


red.source = rb
yellow.source = yb
green.source = gb



while True:
    time.sleep(1)
