import yaml
import ujson as json
import testConfig as config

button = { 'buttonid': 44, 'name': 'YYYYYYY', 'timeout': 2,
          'shortButtonCompletion': 0.5, 'room': 'study', 'location': 'center ceiling',
          'type': 'default'}

units = { 'b8:27:eb:65:16:70':{'host': '192.168.1.47', 'location': 'study', 'other': 'other'},
    'buttons': { 
        21: { 'openmoticid': 50, 'name': 'study center', 'debounce': 0.1, 'timeout': 2, 'shortButtonCompletion': 0.5, 'room': 'study', 'location': 'center ceiling', 'type': 'default' },
        20: { 'openmoticid': 51, 'name': 'study window', 'debounce': 0.1, 'timeout': 2, 'shortButtonCompletion': 0.5, 'room': 'study', 'location': 'center ceiling', 'type': 'default' },
        16: { 'openmoticid': 90, 'name': 'study bench', 'debounce': 0.1, 'timeout': 2, 'shortButtonCompletion': 0.5, 'room': 'study', 'location': 'center ceiling', 'type': 'default' } } }, {
    '00:00:00:00:00:00':{'host': '192.168.1.47', 'location': 'study', 'other': 'other'},
    'buttons': {
        21 : {'openmoticid': 50, 'name': 'study center', 'debounce': 0.1, 'timeout': 2, 'shortButtonCompletion': 0.5, 'room': 'study', 'location': 'center ceiling', 'type': 'default' }, 
        20 : {'openmoticid': 51, 'name': 'study window', 'debounce': 0.1, 'timeout': 2, 'shortButtonCompletion': 0.5, 'room': 'study', 'location': 'center ceiling', 'type': 'default' }, 
        16 : {'openmoticid': 90, 'name': 'study bench', 'debounce': 0.1, 'timeout': 2, 'shortButtonCompletion': 0.5,  'room': 'study', 'location': 'center ceiling', 'type': 'default'
        }
    }
}

mqtt = {'server': 'mqtt.harmers.lightblast.info', 'port': 1884, 'login': '', 'pasword': ''}
openmotics = {'server': '192.168.1.20', 'port': 443, 'login': 'daniel', 'password': 'Ds3ppF14ha'}
version = 1.0

y = config.configuration('./test/testYAML.yml')

us = y.getUnits()
u1 = us['b8:27:eb:65:16:70']
print(u1)




'''
for unitID in y.getUnits():
    unit = y.getUnit(unitID)
    buttons = y.getAllButtons(unitID)
    hostURL = unit['host']
    if y.buttonCount(unitID) != 0:
        for button in buttons:
            print(button)
            print(buttons[button]) 
    

print (y.getUnits())
print('---------------------------------------------------')
print(y.getUnit('00:00:00:00:00:00'))
print('---------------------------------------------------')
print(y.getOpenmoticsServer())
print('---------------------------------------------------')
print(y.getUnit('b8:27:eb:65:16:70'))
print('---------------------------------------------------')

#get all buttons
print(y.getAllButtons('00:00:00:00:00:00'))
print('---------------------------------------------------')

#get one button
print(y.getButton('b8:27:eb:65:16:70',51))
print('---------------------------------------------------')

# add a new button
print(y.buttonExists('b8:27:eb:65:16:70',66))
print(y.buttonCount('b8:27:eb:65:16:70'))
ret = y.addButton('b8:27:eb:65:16:70',66,button)
print(y.buttonCount('b8:27:eb:65:16:70'))
print(y.getAllButtons('b8:27:eb:65:16:70'))
print('---------------------------------------------------')

print(y.sortedButtons('00:00:00:00:00:00'))
print(y.sortedButtons('00:00:00:00:00:00',True))

# test change
print (y.getButton('b8:27:eb:65:16:70',99))
print (y.changeButton('b8:27:eb:65:16:70',99,button))
print (y.getButton('b8:27:eb:65:16:70',99))

# test delete all
#print(y.buttonCount('b8:27:eb:65:16:70'))
#y.delAllButtons('b8:27:eb:65:16:70')
#print(y.buttonCount('b8:27:eb:65:16:70'))

#test delete one button
print(y.buttonCount('b8:27:eb:65:16:70'))
print (y.getButton('b8:27:eb:65:16:70',51))
y.delButton('b8:27:eb:65:16:70',51)
print(y.buttonCount('b8:27:eb:65:16:70'))
print (y.getButton('b8:27:eb:65:16:70',51))
print('---------------------------------------------------')


# get other data
print (y.getVersion())
print (y.getMqttServer())
print (y.getOpenmoticsServer())
print('---------------------------------------------------')

# test setVersion 
print (y.getVersion())
y.setVersion(16.9)
print (y.getVersion())

print('---------------------------------------------------')
# test change mqtt settings
mqttPort = y.getMqttServer()['port']
print (mqttPort)
y.setMqttServer(mqttPort = 9999, mqttServer='testServer')
print (y.getMqttServer())

print('---------------------------------------------------')

# test save data to file
y.setVersion(19.9)
y.save()

y.close()
y = config.configuration('./test/testYAML.yml')
print (y.getUnits())

'''
