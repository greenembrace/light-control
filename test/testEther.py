import os
import socket  
from getmac import get_mac_address 

def getEthName():
        # Get name of the Ethernet interface
            try:
                for root,dirs,files in os.walk('/sys/class/net'):
                    for dir in dirs:
                        if dir[:3]=='enx' or dir[:3]=='eth':
                            interface=dir
            except:
                interface="None"
            return interface
            
def getMAC2(interface='eth0'):
    # Return the MAC address of the specified interface
    try:
        str = open('/sys/class/net/%s/address' %interface).read()
    except:
        try:
            str = get_mac_address()
        except:
            str = "00:00:00:00:00:00"
    return str[0:17]


def getMAC():
  # Return the MAC address of the specified interface
  try:
    for root,dirs,files in os.walk('/sys/class/net'):
        for dir in dirs:
            if dir[:3]=='enx' or dir[:3]=='eth':
                interface=dir
  except:
      interface='eth0'
  try:
      str = open('/sys/class/net/%s/address' %interface).read()
  except:
      try:
          str = get_mac_address()
      except:
          str = "00:00:00:00:00:00"

  return str[0:17]
# Python Program to Get IP Address
def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # doesn't even have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

hostname = socket.gethostname()   
IPAddr = socket.gethostbyname(hostname)   
print("Your Computer Name is:" + hostname)   
print("Your Computer IP Address is:" + IPAddr)   
print (getMAC())
print ('--------------------------------------------')

print(socket.gethostbyname(socket.getfqdn()))
print ('--------------------------------------------')

print(get_ip())
print ('--------------------------------------------')



