#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
import ujson as json

# special loader with duplicate key checking
class UniqueKeyLoader(yaml.SafeLoader):
    def construct_mapping(self, node, deep=False):
        mapping = []
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            #assert key not in mapping
            mapping.append(key)
        return super().construct_mapping(node, deep)

class configuration(UniqueKeyLoader):
  def __init__(self,file):
    self.configFile = file
    with open(self.configFile, 'r') as f:
      self.data = yaml.load(f,Loader=UniqueKeyLoader)

      self.fileHandle = f

  def write(self,file,yamlData):
    # write a new YAML config file
    with open(file, 'w') as f:
        yaml.dump(yamlData, f, sort_keys=False, default_flow_style=False)

  def save(self,unitID = None):
      # write data back to config file
    if  unitID == None:
      with open(self.configFile, 'w') as f:
        self.data['units]'] = self.units
        yaml.dump(self.data, f, sort_keys=False, default_flow_style=False)
    else:
      # write data back to config file
      with open(self.configFile, 'w') as f:
        self.data['units]'][unitID] = self.units[unitID]
        yaml.dump(self.data, f, sort_keys=False, default_flow_style=False)      

  def read(self,file):
    # Read YAML file
    with open(file, 'r') as f:
        return yaml.load(f, Loader=yaml.SafeLoader)

  def update(self,newData):
    # update the current data with a new data set
    try: 
      self.data = newData
      return True
    except:
      return False

  def close(self):
    # close confifuration file
    try:
      self.fileHandle.close()   
      return True
    except:
      return False

  def yaml2Json(self,data,sortkeys=False):
    # output YAML object to JSON format
    try:
      return json.dumps(data, sort_keys=sortkeys)
    except:
      pass

  def json2Dict(self,jsonData):
    # JSON data into Python Dict
    try:  
      return json.load(jsonData)
    except:
      pass

  def getOpenmoticsServer(self):
    # return the openmotics data
    try:
      return self.data['harmers']['openmotics']
    except:
      pass  

  def getMqttServer(self):
    # return the MQTT data
    try:
      return self.data['harmers']['mqttserver']
    except:
      pass

  def getUnits(self):
    # return all units information 
    try:
      return self.units
    except:
      pass

  def getUnit(self,unitID):
      # return unit with unitID 
      try:
        return self.units[unitID]
      except:
        pass

  def getVersion(self):
    # return the data version
    try:
      return self.data['harmers']['version']
    except:
      pass

  def setVersion (self,version):
    # set the version value
    try:
      self.data['harmers']['version'] = version
      return True
    except:
      return False

  def setMqttServer (self, mqttServer='', mqttPort=0, mqttLogin = '', mqttPassword = ''):
    # set the mqtt data
    try:
      mqtt = self.getMqttServer()
      if mqttServer != '':
        mqtt['server'] = mqttServer
      if mqttPort > 0:
        mqtt['port'] = mqttPort
      if mqttLogin != '':
        mqtt['login'] = mqttLogin
      if mqttPassword != '':
        mqtt['password'] = mqttPassword    

      self.data['harmers']['mqttserver'] = mqtt
    except:
      pass

  def setOpenmoticsServer (self, openmoticsServer='', openmoticsPort=0, openmoticsLogin = '', openmoticsPassword = ''):
    # set the mqtt data
    try:
      openmotics = self.getOpenmoticsServer()
      if openmoticsServer != '':
        openmotics['server'] = openmoticsServer
      if openmoticsPort > 0:
        openmotics['port'] = openmoticsPort
      if openmoticsLogin != '':
        openmotics['login'] = openmoticsLogin
      if openmoticsPassword != '':
        openmotics['password'] = openmoticsPassword 

      self.data['harmers']['openmotics'] = openmotics
    except:
      pass

  def setUnitInfo (self, unitID, host='', location='', other=''):
      # set the unitInfo data
      try:
        unitInfo = self.getUnit()
        if host != '':
          unitInfo['unitID'] = host
        if location != '':
          unitInfo['location'] = location
        if other != '':
          unitInfo['other'] = other 
            
        self.units[unitID]['unit'] = unitInfo    
      except:
        pass
  
  def getAllButtons(self,unitID):
    # get all the buttone or 'None' if they don't exist
    try:
      buttons = self.units[unitID]['buttons']
      return buttons
    except:
      return None

  def getButton(self,unitID,button):
    # return the specified button or 'None' if it does not exist
    try:
      button = self.units[unitID]['buttons'][button]
      return button
    except:
      return None

  def delButton(self,unitID,button):
    # delete a button from file
    try:
      self.units[unitID]['buttons'].pop(button)
      return True
    except:
      return False  

  def delAllButtons(self,unitID):
    # delete all the button from file
    try:
      self.units[unitID].pop('buttons')
      return True
    except:
      return False  

  def changeButton(self,unitID,buttonName,newButton):
    # change a single button
    if self.delButton(unitID,buttonName):
      self.addButton(unitID,buttonName,newButton)
      return True
    else:
      return False

  def copyButton(self,unitID,buttonName1,buttonName2):
    # copy details of button1 to button2
    b1 = self.getButton(unitID,buttonName1)
    return self.addButton(unitID,buttonName2,b1)

  def addButton(self,unitID,buttonName,button):
    # add a new button
    if not self.buttonExists(unitID,buttonName):
      self.units[unitID]['buttons'][buttonName] = button
      return True
    else:
      return False  

  def buttonCount(self,unitID):
    # return the number of buttons
    try:
      count =  len(self.units[unitID]['buttons']) 
    except:
      return 0
    else:
      return count

  def buttonExists(self,unitID,buttonName):
    # check if button exists in buttons
    try: 
      return buttonName in self.units[unitID]['buttons']  
    except:
      return False
      
  def sortedButtons(self,unitID,direction = False):
    # returns a sorted list of button names. Optional direction, default descending
    return sorted(self.units[unitID]['buttons'],reverse = direction)

