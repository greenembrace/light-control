#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import yaml
import ujson as json
from  pythonping import ping
from gpiozero import LED
from gpiozero.pins.pigpio import PiGPIOFactory

# special loader with duplicate key checking
class UniqueKeyLoader(yaml.SafeLoader):
    def construct_mapping(self, node, deep=False):
        mapping = []
        for key_node, value_node in node.value:
            key = self.construct_object(key_node, deep=deep)
            assert key not in mapping, 'duplicate entries in configuration, area around : ' + str(key)
            mapping.append(key)
        return super().construct_mapping(node, deep)

class configuration():
  def __init__(self,file):
    self.configFile = file
    with open(self.configFile, 'r') as f:
      self.data = yaml.load(f, Loader=UniqueKeyLoader)
      self.uNodes = self.data['uNodes']
      self.fileHandle = f

  # ************************************************************
  # File manipulation
  # ************************************************************
  
  def write(self,file,yamlData):
    # write a new YAML config file
    with open(file, 'w') as f:
        yaml.dump(yamlData, f, sort_keys=False, default_flow_style=False)

  def save(self):
      return
      self.close()
      # write data back to config file. return true if successful
      self.data['uNodes'] =  self.uNodes
      try:
          with open(self.configFile, 'w') as f:
            yaml.safe_dump(self.data, f, sort_keys=False, default_flow_style=False)
            return True
      except:
        return False      

  

  def read(self,file):
    # Read YAML file
    with open(file, 'r') as f:
        return yaml.load(f, Loader=yaml.SafeLoader)

  def update(self,newData):
    # replace the current data with a new data set
    try: 
      self.data = newData
      return True
    except:
      return False

  def close(self):
    # close configuration file
    try:
      self.fileHandle.close()   
      return True
    except:
      return False

  # *************************************************************
  # tools
  # *************************************************************

  def yaml2Json(self,data,sortkeys=False):
    # output YAML object to JSON format
    try:
      return json.dumps(data, sort_keys=sortkeys)
    except:
      pass

  def json2Dict(self,jsonData):
    # JSON data into Python Dict
    try:  
      return json.load(jsonData)
    except:
      pass
  
  # ***************************************************************
  # Configuration Header
  # ***************************************************************

  def getOpenmoticsServer(self):
    # return the openmotics data
    try:
      return self.data['harmers']['openmotics']
    except:
      return False  

  def getMqttServer(self):
    # return the MQTT data
    try:
      return self.data['harmers']['mqttserver']
    except:
      return False

  def getVersion(self):
    # return the data version
    try:
      return self.data['harmers']['version']
    except:
      return False

  def setVersion (self,version):
    # set the version value
    try:
      self.data['harmers']['version'] = version
      return True
    except:
      return False

  def setOpenmoticsServer (self, openmoticsServer='', openmoticsPort=0, openmoticsLogin = '', openmoticsPassword = ''):
    # set the mqtt data
    try:
      openmotics = self.getOpenmoticsServer()
      if openmoticsServer != '':
        openmotics['server'] = openmoticsServer
      if openmoticsPort > 0:
        openmotics['port'] = openmoticsPort
      if openmoticsLogin != '':
        openmotics['login'] = openmoticsLogin
      if openmoticsPassword != '':
        openmotics['password'] = openmoticsPassword 

      self.data['harmers']['openmotics'] = openmotics
    except:
      return False    

  def getDebounce(self):
    return self.data['harmers']['debounce']
  
  def getTimeout(self):
    return self.data['harmers']['timeout']
  
  def getShortButtonCompletion(self):
    return self.data['harmers']['shortButtonCompletion']

  def getLongButtonCompletion(self):
    return self.data['harmers']['longButtonCompletion']  

  # **************************************************************
  # uNode manipulation
  # **************************************************************

  def getUnodes(self):
    # return all uNode information 
    try:
      return self.uNodes
    except:
      return False

  def getUnode(self,uNodeID):
    # return uNode with uNodeID 
    try:
      return self.uNodes[uNodeID]
    except:
      return False

  def unodeExists(self,uNodeID):
    # return uNode with uNodeID 
    try:
      return True if uNodeID in self.uNodes else False
    except:
      return False 
  
  def updateUnode(self, uNodeID, hostIP=''):
    # update the unodeInfo data
    try:
      unitInfo = self.getUnode(uNodeID)
      if hostIP != '':
        unitInfo['hostip'] = hostIP

      self.uNodes[uNodeID]['uNode'] = unitInfo    
    except:
      return False

  def getPinList(self,uNodeID):
    # return the valid and available pins
    pinList = {}
    try:
      unitInfo = self.getUnode(uNodeID)
      pinList = unitInfo['pins'].keys()
      return pinList
    except:
      return False

  def getStartPin(self, uNodeID):
    unitInfo = self.getUnode(uNodeID)
    return unitInfo['startPin']
    
  def getEndPin(self, uNodeID):
    unitInfo = self.getUnode(uNodeID)
    return unitInfo['endPin']

  def getReservedGPIOPins(self, uNodeID):
    unitInfo = self.getUnode(uNodeID)
    return unitInfo['reservedGPIOPins']    

  def addNewUnode(self,uNodeID,unitIP):
    # create a new uNode. return True if created else False
    if self.unodeExists(uNodeID): return False
    uNode = {}
    uNode['hostip'] = {}
    uNode['hostip'] = unitIP
    uNode['pins'] = ''
    self.uNodes[uNodeID] = {}        
    self.uNodes[uNodeID] = uNode
    return True if self.save() else False
    
    
  # *************************************************************
  # Button Manipulation
  # *************************************************************

  def addNewButton(self,uNodeID,buttonPins):
    # adds new button with defaults
    for pin in buttonPins:
      button = {}
      button['openmoticsID'] = ''
      self.addButton(uNodeID,pin,button)

  def addButton(self,uNodeID,buttonName,button):
      # add a new button with supplied data
      if not self.buttonExists(uNodeID,buttonName):
        self.uNodes[uNodeID]['pins'][buttonName] = button
        return True
      else:
        return False

  def getAllButtons(self,uNodeID):
    # get all the buttone or 'None' if they don't exist
    try:
      return self.uNodes[uNodeID]['pins']
    except:
      return False

  def getButton(self,uNodeID,button):
    # return the specified button or 'None' if it does not exist
    try:
       return self.uNodes[uNodeID]['ons'][button]
    except:
      return False

  def delButton(self,uNodeID,button):
    # delete a button from file
    try:
      self.uNodes[uNodeID]['pins'].pop(button)
      return True
    except:
      return False  

  def delAllButtons(self,uNodeID):
    # delete all the button from file
    try:
      self.uNodes[uNodeID].pop('pins')
      return True
    except:
      return False  

  def changeButton(self,uNodeID,buttonName,newButton):
    # change a single button
    if self.delButton(uNodeID,buttonName):
      self.addButton(uNodeID,buttonName,newButton)
      return True
    else:
      return False

  def copyButton(self,uNodeID,buttonName1,buttonName2):
    # copy details of button1 to button2
    b1 = self.getButton(uNodeID,buttonName1)
    return self.addButton(uNodeID,buttonName2,b1)

  def buttonCount(self,uNodeID):
    # return the number of buttons
    try:
      count =  len(self.uNodes[uNodeID]['pins'].keys()) 
    except:
      return 0
    else:
      return count

  def buttonExists(self,uNodeID,buttonName):
    # check if button exists in buttons
    try:
      button = True if buttonName in self.uNodes[uNodeID]['pins'] else False 
      return button
    except:
      return False

  def updateButtons(self,uNodeID,buttons):
    # reload buttons into unode
    try:
      self.uNodes[uNodeID]['pins'] = buttons
      return True
    except:
      return False
     
  def sortedButtons(self,uNodeID,direction = False):
    # returns a sorted list of button names. Optional direction, default descending
    return sorted(self.uNodes[uNodeID]['pins'],reverse = direction)

# ******************************************************************************
# T O O L S
# ******************************************************************************

  def getReservedPin (self, descriptor, unodeID):
        reservedPins = self.getReservedGPIOPins(unodeID)
        v = list(reservedPins.values()) 
        k = list(reservedPins.keys())
        return k[v.index(descriptor)]

  def getLED(self,descriptor, unodeID):
      activityLED = self.getReservedPin(descriptor, unodeID)
      hostIP = self.getUnode(unodeID)['hostip']
      factory = PiGPIOFactory(host=hostIP)
      return LED(pin=activityLED, pin_factory=factory)

  def checkPin(self,bPin, bUnit, pinList):
    if not type(bPin) is int:
      raise ValueError ('Pin on: ' + bUnit + ' : [' +str(bPin) +'] is not "INTEGER"')
        
    if not bPin in range(2,27):  
      raise ValueError ('Pin on: ' + bUnit + ' : [' +str(bPin) +'] is "OUT OF RANGE"')
    
    if not bPin in pinList:  
      raise ValueError ('Pin on: ' + bUnit + ' : [' +str(bPin) +'] is not "VALID"')
    

  def checkOM(self,bOM, bUnit, bPin):
    if not type(bOM) is int:
        raise ValueError ('Openmotics Input: (' + bUnit + ' : ' + str(bPin) + ')[' + str(bOM) + '] is not Integer') 
          
    if not bOM in range (0,103):
      raise ValueError ('Openmotics Input: (' + bUnit + ' : ' + str(bPin) + ')[' + str(bOM) + '] is "OUT OF RANGE"')

  def checkHost(self,hostIP):
    try:
      ping(hostIP,count=5)
    except:
      raise ValueError ('invalid uNode: (' + hostIP + ') Possibly NOT online')

  def parameterValidation(self, hostIP, bUnit, bPin, bOM, pinList):
    self.checkPin(bPin, bUnit, pinList)
    self.checkOM(bOM, bUnit, bPin)
    self.checkHost(hostIP)