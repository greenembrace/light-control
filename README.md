
# Harmers Ancillary Light button controller

The Harmers Ancillary Light (HAL) button controller is an adjunct to the existing Openmotics CAN control. It allows for additional buttons to be added to the Harmers home automation system.

The system consists of the HAL controller and zero or more satellite button unit node (uNode). Each uNode unit consists of a Raspberry Pi (RPi) with some additional hardware for the attachment of buttons and management of debounce conditions.

The HAL controller runs the primary driver software that reads a YAML configuration file for the definitions of the buttons. It then establishes a communication connection between itself and the uNode and waits for button activity. When a button is pressed the activity is signed back to the HAL which in turn forwards a condition change to the Openmotics system.

The HAL Controller can run on any computer that supports the Python programming language. However if HAL also runs on a RPi it can also act as a uNode.

[[_TOC_]]

## HAL Controller:

The HAL controllers primary task is to establish a link between the button connected to the uNode and the Openmotics system that in turn allows for the control of a lights, pumps, fans or other devices. 

HAL achieves this link by reading a button definition from the YAML configuration file which it uses to setup a button thread that in itself establishes a connection with the uNode. Each button lives in its own thread and establishes existence by publishing its pin details mqtt:tele/controller/[MAC]/unitPins/[pin], [MAC] is the MAC address of the uNode, [pin] is the io-pin number that button is connected too.

The Active/In-active status of each button is published to mqtt:tele/controller/[MAC]/buttons/[button]/LWT, [button] is the Openmotics number associated with the connection.

Once HAL has established all the buttons threads, it monitors them for any failures which are reported via mqtt. It also listens for any incoming commands. If a thread fails for some reason then the In-active status is published.

### Harware:

HAL Controller is hardware independent. HAL could co-habitat on a Raspberry Pi 3 or latter with the uNode. The hardware needs some way of communicating with the bNodes, mqtt server and Openmotics server. No connection to the Internet is required. Either WiFi or ethernet are fine however Ethernet is the preferred method to reduce latency.
### Software:
`HControl.py`

### Communications:

While access to the MQTT server is preferred, HAL will proceeded with attempting to create button threads without knowing if the uNode is 'online'. The uNode hardware may be running (ping is insufficient) but the software may not be running or corrupt. There may be more error messages during this process. `DControl.py` will continue to trying to connect to the MQTT server.

HAL checks the controller/Discovery[MAC]/enabled flag for true/false status. If this flag is set to true then the uNode's button entries in the YAML file will be read and processed otherwise the entries will be ignored. This flag can be set to false to disable the uNode.
## uNode:

  ### hardware:

  The uNode consist of two hardware components and two software components.
  The hardware components are the RPi, any model 3 or above running standard OS. The second hardware component attached to the RPi's 40 pin header, is the button connection card (BCC). These two components are connected through a 40 pin ribbon cable. This BCC allows for the easy connection of buttons via screw terminals, it also allows for the attachment of up to 16 buttons. 

  The additional hardware also manages button debounce conditions that may arise through poor connection and electrical characteristics. There is also the possible future extension for button LED control.

  BCC also supports either momentary or toggle styles buttons.
  ### software:

  The two software components are: IO management daemon that watches and controls the RPi's io-pins, the software is a third-party program - [pigpio](https://abyz.me.uk/rpi/pigpio/index.html). pigpio is a sophisticated and very fast io-drive, written in native 'C'.  

  The pigpio daemon communicates  directly with the HAL controller software, through port :8888. The RPi's remote GPIO interface is required to be opened, this can be done through #sudo raspi-config in the interfaces tab.

  The second piece of software: `HDiscovery.py` a simple discovery program which has two main tasks, the first to start and stop the pigpio daemon and the second is to advertise its own existence. This program communicates with HAL via mqtt. This program starts automatically when the RPi boots, may be reboot via the HAL or MQTT message.

  ![Discovery](images/Discovery.png "MQTT Discovery")
  
  Its existence is established by the publication of a configuration string to mqtt:controller/discovery/MAC/config where MAC is the MAC address of the device. The MAC is used as the primary ID to uniquely identify the different bNodes units.

  It will also publish a a last will and testament (LWT) to establish its online/offline status to mqtt:tele/controller/MAC/LWT
 
## YAML Configuration

The `DControl.yml` file stores the required details for each uNode. It separated in two parts, the first is system details including the location of the Openmotics server. The second part are the entries for each uNode. The `uNodes:` are uniquely identified be their `[MAC]:` address, each has some header details and then a section describing each button. 

The header includes the `hostip:`, the IP address of the unit, this is used make the connection to the uNode, and a description of its `location:`. The `buttons:` section are the entries for each button that is connected to the uNode. each entry starts with a unique identifier `[name]:`, this name can be anything but should be something that assists. 

`buttonPin:` is the Raspberry Pi Pin number based on Broadcom pin numbering not the physical location on the RPi, however the screw terminals on the uNode will be in numeric order. The valid range is an Integer [`2..27`]

`openmoticsID:` is the associated Openmotics output number, these numbers can be found in the Openmotics console under Outputs. The valid range is an Integer [`0..95`]

`debounce:` if a time period, to stabilise the electrical value of the buttons input. Buttons and switches tend to have noisy signals that can cause multiple on/off conditions. The debounce waits-out this noisy period. The value is a positive Float 1.0 being 1 second but generally described in micro-seconds. Default = [`0.1`] this can be change in the [harmers:] header.

`timeout:` Time to wait in seconds to see if the button is still being pressed. Default = [`2.0`] this can be change in the [harmers:] header.

`shortButtonCompletion:` Maximum time in seconds to complete a press. Default = [`0.5`] this can be change in the [harmers:] header.


```yaml
harmers:
  openmotics:
    login: applogin
    password: Ds3+pF14app
    port: 443
    server: 192.168.1.20
  name: shower room  
  version: 21.0
unode:
  debounce: 0.05
  dimmerPressCompletion: 1.0
  dimmerSpeed: 50
  endPin: 27
  longButtonCompletion: 0.95
  pins:
    5: {physical: 29, dimmerID: 0, multiPress: false, openmoticsID: 16, type: light} # shower rm. center
    12: {physical: 32, dimmerID: 0, multiPress: false, openmoticsID: 31, type: light} # west spot 1
    13: {physical: 33, dimmerID: de1a77b5-141e-4ca8-9574-7658a0a8c128, multiPress: false, openmoticsID: 22, type: light} # shower mirror
    19: {physical: 35, dimmerID: 0, multiPress: false, openmoticsID: 54, type: light} # shower fan
    26: {physical: 37, dimmerID: 0, multiPress: false, openmoticsID: 61, type: light} # bedroom. 1 low
    18: {physical: 12, dimmerID: 0, multiPress: false, openmoticsID: 19, type: light} # bedroom. 1 high
    16: {physical: 36, dimmerID: 0, multiPress: false, openmoticsID: 64, type: light} # west spot 2
    25: {physical: 22, dimmerID: 0, multiPress: false, openmoticsID: 65, type: light} # all off
    20: {physical: 38, dimmerID: 0, multiPress: false, openmoticsID: 92, type: light} # shutter up
    21: {physical: 40, dimmerID: 0, multiPress: false, openmoticsID: 93, type: light} # shutter down
  reservedGPIOPins:
    Rx: 16
    Tx: 14
    activityLED: 17
    list:
      2: null
      3: null
      4: null
      14: null
      16: null
      17: null
    powerLED: 4
    sda: 2
    sdl: 3
  shortButtonCompletion: 0.4
  startPin: 2
7777
```