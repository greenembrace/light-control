#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from getmac import get_mac_address 
import socket

class identity():
    def __init__(self):
        self.unodeID = self.getMAC()
        self.hostname = socket.gethostname()   
        self.unodeIP = self.getIP()
        self.unodeURL = socket.getfqdn()

    def getIP(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            s.connect(('10.255.255.255', 1))
            IP = s.getsockname()[0]
        except Exception:
            IP = '127.0.0.1'
        finally:
            s.close()
        return IP

    def getMAC(self):
        # Return the MAC address of the specified interface
        interface='eth0'
        try:
            for root,dirs,files in os.walk('/sys/class/net'):
                for dir in dirs:
                    if dir[:3]=='enx' or dir[:3]=='eth':
                        interface=dir
        except:
            interface='eth0'
        try:
            str = open('/sys/class/net/%s/address' %interface).read()
        except:
            try:
                str = get_mac_address()
            except:
                str = "00:00:00:00:00:00"
        return str[0:17]